## 1.3.0.0

- Add reset button to reset input
- Bug fix Culture thousand separator, where the program breaks if dot is used as separator

## 1.2.0.0

- Add auto comma for thousang separator
- Prevent symbol input
- Prevent alphabet input

## 1.1.0.0

- Change console into WPF

## 1.0.0.0

- Finalized console number to string