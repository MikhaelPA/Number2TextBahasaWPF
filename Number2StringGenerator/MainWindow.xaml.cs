﻿/*
 *  Number2TextBahasaWPF
    Copyright (C) 2018  Mikhael Pramodana Agus

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 * 
 */

using System;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace Number2StringGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// track length change
        /// since v1.1
        /// </summary>
        private static int initLength = 0;
        private static readonly Regex regEx = new Regex(@"^(0|[1-9][0-9]*)$");

        /// <summary>
        /// Main Method that runs on execution of program
        /// </summary>
        public MainWindow()
        {
            // Initialize Component WPF
            InitializeComponent();

            // Set focus on input text box
            InputNumber.Focus();

            // v1.1
            LblVersion.Text = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        /// <summary>
        /// Generate Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGenerate_Click(object sender, RoutedEventArgs e)
        {
            //Run method number to string
            Number2String();
        }


        /// <summary>
        /// Input number textbox event on Key Up, check in input textbox not empty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputNumber_KeyUp(object sender, KeyEventArgs e)
        {
            // Throw other input other than number and comma 
            // v1.1
            // Obsolete as of v1.2
            //StringReject();

            var inputLength = NumberNormalize(InputNumber.Text).Length;
            // separate thousand number mark
            // v1.1
            if (initLength != inputLength)
            {
                initLength = inputLength;
                NumberSeparator();
            }

            // If input box not empty, enabled button 
            // v1.0
            if (!string.IsNullOrEmpty(InputNumber.Text))
            {
                BtnGenerate.IsEnabled = true;
            }
            // If input box empty, disabled button 
            // v1.0
            else
            {
                BtnGenerate.IsEnabled = false;
            }
        }

        /// <summary>
        /// Separate thousand with comma ( , )
        /// since v1.1
        /// </summary>
        private void NumberSeparator()
        {
            var tempString = "";

            // normalize text to number only
            var normalizedString = NumberNormalize(InputNumber.Text);

            if (!string.IsNullOrEmpty(normalizedString))
            {
                if (InputNumber.Text.Length > 1)
                {
                    // input comma
                    tempString = long.Parse(normalizedString).ToString("#,###");

                    // save caret position
                    var start = InputNumber.SelectionStart;
                    if (InputNumber.SelectionStart > tempString.Length)
                    {
                        start = tempString.Length;
                    }
                    else if (InputNumber.SelectionStart < tempString.Length)
                    {
                        var dif = tempString.Length - InputNumber.Text.Length;
                        start = InputNumber.SelectionStart + dif;
                    }
                    var length = InputNumber.SelectionLength;

                    InputNumber.Text = tempString;

                    // restore caret position
                    InputNumber.Select(start, length);
                }
            }
        }

        [Obsolete("As of v1.2 Please refer to PreviewTextInput")]
        /// <summary>
        /// Reject any input other than 0-9 and comma
        /// OBSOLETE AS OF v1.2
        /// since v1.1
        /// </summary>
        private void StringReject()
        {
            var tempString = "";

            for (var index = 0; index < InputNumber.Text.Length; index++)
            {
                if (regEx.IsMatch(InputNumber.Text[index].ToString()))
                {
                    tempString += InputNumber.Text[index];
                }
            }
            InputNumber.Text = tempString;

        }

        /// <summary>
        /// Normalize inputted number from comma separator
        /// v1.1 => v1.2
        /// </summary>
        /// <param name="inputedString"></param>
        /// <returns></returns>
        private string NumberNormalize(string inputedString)
        {
            var splitStr = inputedString.Split(',', '.');

            var normalizedString = "";
            for (var index = 0; index < splitStr.Length; index++)
            {
                normalizedString += splitStr[index];
            }
            return normalizedString;
        }

        /// <summary>
        /// Validate Input Before generate
        /// </summary>
        private void Number2String()
        {
            // since v1.1
            var normalizeNumber = NumberNormalize(InputNumber.Text);

            //Check if input exceeds 15 character, return error message
            if (normalizeNumber.Length > 15)
            {
                ResultBox.Text = "Out of range, please input less number";

            }
            //Check if input a number, return string representation of inputted number
            else if (ulong.TryParse(normalizeNumber, out var result))
            {
                ResultBox.Text = NumberToStringGenerator(normalizeNumber);
            }
            //Check if input not a number, return error message
            else
            {
                ResultBox.Text = "Error, please input number only";
            }
        }

        /// <summary>
        /// Number to string method
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string NumberToStringGenerator(string number)
        {
            var numberString = "";
            var flagThreeSeparator = false;
            if (number.Length > 1)
            {
                for (var index = 0; index < number.Length; index++)
                {
                    var tempNum = int.Parse(number.Substring(index, 1));

                    // Check if number one or not ( i.e se-### )
                    if (IsNumberOneAboveTen(tempNum, index, number.Length))
                    {
                        // last 2 digit ( i.e 10 (sepuluh) up to 19 (sembilan belas) )
                        if (index == number.Length - 2)
                        {
                            tempNum = int.Parse(number.Substring(index, 2));
                            numberString += GetNumberBelasString(tempNum);
                            break;
                        }
                        // last 2 digit ( i.e 10 (sepuluh) up to 19 (sembilan belas) ) after thousand
                        if ((number.Length - (index + 1)) % 3 == 1)
                        {
                            tempNum = int.Parse(number.Substring(index, 2));
                            index++;
                            numberString += $"{GetNumberBelasString(tempNum)} { GetNumberSeparatorStringV2(index, number.Length)}";

                        }
                        // thousand separator with one as the beginning ( i.e seratus and seribu)
                        else
                        {
                            numberString += $"SE{GetNumberSeparatorStringV2(index, number.Length)}";
                        }
                        flagThreeSeparator = false;
                    }
                    // Skip zero
                    else if (tempNum != 0)
                    {
                        numberString += $"{GetNumberString(tempNum)} {GetNumberSeparatorStringV2(index, number.Length)}";
                        flagThreeSeparator = false;

                    }
                    // thousand separator
                    else if ((number.Length - (index)) % 3 == 1 && index != number.Length - 2 && flagThreeSeparator == false && !string.IsNullOrEmpty(numberString))
                    {
                        numberString += $"{GetNumberSeparatorStringV2(index, number.Length)} ";
                        flagThreeSeparator = true;
                    }

                    // ADD ON
                    // Put Space between words
                    if (index + 1 < number.Length && tempNum != 0)
                    {
                        numberString += " ";
                    }
                    // Set flag thousand separator after first appearance
                    if ((number.Length - (index)) % 3 == 1)
                    {
                        flagThreeSeparator = true;
                    }
                }
            }
            else if (number.Length == 1)
            {
                var tempNumb = int.Parse(number.Substring(0, 1));

                numberString += GetNumberString(tempNumb);
            }
            return numberString;
        }

        /// <summary>
        /// Check number one position
        /// </summary>
        /// <param name="number"></param>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private bool IsNumberOneAboveTen(int number, int index, int length)
        {

            var numb = length - (index + 1);
            if (number == 1)
            {
                if (numb % 3 == 1 || numb % 3 == 2)
                {
                    return true;
                }
                else if (numb == 3 && length == 4)
                {
                    //thousand
                    return true;
                }
                else if (numb == 3 && length > 4)
                {
                    //thousand
                    return false;
                }
                else if (numb == 6)
                {
                    //million
                    return false;
                }
                else if (numb == 9)
                {
                    //billion
                    return false;
                }
                else if (numb == 12)
                {
                    //trillion
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Get string of normal number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string GetNumberString(int number)
        {
            switch (number)
            {
                case 0:
                    return "NOL";
                case 1:
                    return "SATU";
                case 2:
                    return "DUA";
                case 3:
                    return "TIGA";
                case 4:
                    return "EMPAT";
                case 5:
                    return "LIMA";
                case 6:
                    return "ENAM";
                case 7:
                    return "TUJUH";
                case 8:
                    return "DELAPAN";
                case 9:
                    return "SEMBILAN";
                default: return "";
            }
        }

        /// <summary>
        /// Get string of number for last 2 digit starting with 10-19
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string GetNumberBelasString(int number)
        {
            switch (number)
            {
                case 10:
                    return "SEPULUH";
                case 11:
                    return "SEBELAS";
                case 12:
                    return "DUA BELAS";
                case 13:
                    return "TIGA BELAS";
                case 14:
                    return "EMPAT BELAS";
                case 15:
                    return "LIMA BELAS";
                case 16:
                    return "ENAM BELAS";
                case 17:
                    return "TUJUH BELAS";
                case 18:
                    return "DELAPAN BELAS";
                case 19:
                    return "SEMBILAN BELAS";
                default: return "";
            }
        }

        /// <summary>
        /// Get number separator up to trillion
        /// </summary>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GetNumberSeparatorStringV2(int index, int length)
        {
            var numberOfZero = length - (index + 1);

            if (numberOfZero % 3 == 2)
            {
                return "RATUS";
            }
            else if (numberOfZero % 3 == 1)
            {
                return "PULUH";
            }
            else
            {
                switch (numberOfZero)
                {
                    case 3: return "RIBU";
                    case 6: return "JUTA";
                    case 9: return "MILIAR";
                    case 12: return "TRILIUN";
                        //case 15: return "KUADRILIUN";
                        //case 18: return "KUANTILIUN";
                        //case 21: return "SEKSTILIUN";
                        //case 24: return "SEPTILIUN";
                        //case 27: return "OKTILIUN";
                        //case 30: return "NONILIUN";
                        //case 33: return "DESLIUN";
                }
            }
            return "";
        }

        /// <summary>
        /// Prevent other character to be inputted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !regEx.IsMatch(e.Text);
            if (!string.IsNullOrEmpty(InputNumber.Text))
            {
                e.Handled = !regEx.IsMatch(NumberNormalize(InputNumber.Text) + e.Text);
            }
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text = string.Empty;
        }
    }
}
